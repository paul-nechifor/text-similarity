import json
import sys

import click

from .similarity import compute_similarity


@click.command()
@click.option("--topics", type=int, default=9)
@click.argument("texts", type=click.File("rb"), default=sys.stdin)
@click.argument("output", type=click.File("wb"), default=sys.stdout)
def main(topics, texts, output):
    """Compute the similarity between all the texts."""
    text_list = json.load(texts)
    similarity_matrix = compute_similarity(text_list, topics)
    json.dump(similarity_matrix, output)


if __name__ == "__main__":
    main()
