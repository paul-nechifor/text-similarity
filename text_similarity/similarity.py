import re
from collections import defaultdict

import nltk
from gensim import corpora, models, similarities
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer

stops = set(stopwords.words("english"))
stemmer = SnowballStemmer("english")


def compute_similarity(documents, num_topics):
    # Remove common words and tokenize.
    texts = [
        [
            stemmer.stem(word)
            for word in nltk.word_tokenize(document.lower())
            if word not in stops and bool(re.search(r"\w", word, re.U))
        ]
        for document in documents
    ]

    # Remove words that appear only once.
    frequency = defaultdict(int)
    for text in texts:
        for token in text:
            frequency[token] += 1
    texts = [[token for token in text if frequency[token] > 1]
             for text in texts]

    dictionary = corpora.Dictionary(texts)
    corpus = [dictionary.doc2bow(text) for text in texts]

    lsi = models.LsiModel(corpus, id2word=dictionary, num_topics=num_topics)
    index = similarities.MatrixSimilarity(lsi[corpus])

    return [[float(x) for x in index[lsi[corp]]] for corp in corpus]
