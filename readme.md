# Text Similarity

Compute the text similarity between multiple texts.

```bash
echo '["The cat is good.", "Cats do not fly.", "Dogs bark. Loudly!"]' |
poetry run python -m text_similarity

[[1.0, 1.0, 0.0], [1.0, 1.0, 0.0], [0.0, 0.0, 0.0]]
```
